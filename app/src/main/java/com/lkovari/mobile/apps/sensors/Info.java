package com.lkovari.mobile.apps.sensors;



import java.util.Locale;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.BatteryManager;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;


/**
 * 
 * @author lkovari
 *
 */
public class Info extends Activity {
	private TextView batteryLevelTitleTextView;
	private TextView batteryLevelValueTextView;
	private boolean isBatteryChangeReceiversRegistered = false;
	
	/**
	 * 
	 */
	private final BroadcastReceiver batteryChangedIntentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
	        if (Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {
	            int batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
	            double batteryVoltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0) / 1000;
	            double batteryTemperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10;
	            
	             int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_UNKNOWN);
	             String batteryStatus = null;
	             if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
	                 batteryStatus = getResources().getString(R.string.battery_status_charging);
	             } else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
	                 batteryStatus = getResources().getString(R.string.battery_status_discharging);
	             } else if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
	                 batteryStatus = getResources().getString(R.string.battery_status_notcharging);
	             } else if (status == BatteryManager.BATTERY_STATUS_FULL) {
	                 batteryStatus = getResources().getString(R.string.battery_status_full);
	             } else {
	                 batteryStatus = getResources().getString(R.string.battery_status_notavailable);
	             }
	            
	             int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, BatteryManager.BATTERY_HEALTH_UNKNOWN);
	             String batteryHealth = null;
	             if (health == BatteryManager.BATTERY_HEALTH_GOOD){
	            	 batteryHealth = getResources().getString(R.string.battery_health_good);
	             } else if (health == BatteryManager.BATTERY_HEALTH_OVERHEAT){
	            	 batteryHealth = getResources().getString(R.string.battery_health_overheat);
	             } else if (health == BatteryManager.BATTERY_HEALTH_DEAD){
	            	 batteryHealth = getResources().getString(R.string.battery_health_dead);
	             } else if (health == BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE){
	            	 batteryHealth = getResources().getString(R.string.battery_health_overvoltage);
	             } else if (health == BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE){
	            	 batteryHealth = getResources().getString(R.string.battery_health_unspecifiedfailure);
	             } else{
	            	 batteryHealth = getResources().getString(R.string.battery_health_notavailable);
	             }
	             
	             if ((batteryStatus != null) || (batteryHealth != null))
	            	 batteryLevelTitleTextView.setText(getResources().getString(R.string.battery_text) + ": " + batteryHealth + ", " + batteryStatus);
	             
	            // always greater then -1
	            if (batteryLevel > -1) 
	            	batteryLevelValueTextView.setText(" " + batteryLevel + "% " + String.format("%.02fV", batteryVoltage) + " " + String.format("%.02fC", batteryTemperature));
	        }
		}
		
	};
	
	
	private void displayVersionName() {
	    String versionName = "";
	    String versionCode = "";
	    PackageInfo packageInfo;
	    try {
	        packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
	        versionName = "v " + packageInfo.versionName;
	        versionCode = " #" + packageInfo.versionCode;
	        
	    } catch (NameNotFoundException e) {
	        e.printStackTrace();
	    }
	    TextView tv = (TextView) findViewById(R.id.aboutVersionLabelValueTextView);
	    tv.setText(versionCode + " " + versionName);
	}	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		

		batteryLevelTitleTextView = (TextView)findViewById(R.id.aboutBatteryLabelTextView);
		batteryLevelValueTextView = (TextView)findViewById(R.id.aboutBatteryLabelValueTextView);
		
		// 01/08/2013
		displayVersionName();
		
        /*
        ImageView imageView = (ImageView) findViewById(R.id.logoImageView);
        imageView.setImageResource(R.drawable.icon);

        TextView appnameTextView = (TextView) findViewById(R.id.aboutApplicationLabelValueTextView);
        appnameTextView.setText(R.string.about_application_label_value);

        TextView descriptionTextView = (TextView) findViewById(R.id.aboutDescriptionLabelValueTextView);
        descriptionTextView.setText(R.string.about_description_label_value);
        TextView authorValueTextView = (TextView) findViewById(R.id.aboutAuthorLabelValueTextView);
        String langCode = Locale.getDefault().getISO3Language();
        String company = "EKLSoft Trade Llc.";
        if (langCode.toUpperCase() == "HU") {
        	company = "EKLSoft Trade Kft.";
        }
        authorValueTextView.setText(Html.fromHtml("<a href='http://www.eklsofttrade.com'>"+company+"</a>"));
		*/
    }
    
    
    @Override
    protected void onResume() {
		  IntentFilter intentToReceiveFilter = new IntentFilter();
		  intentToReceiveFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
//		  this.registerReceiver(batteryChangedIntentReceiver, intentToReceiveFilter, null, handler);
		  this.registerReceiver(batteryChangedIntentReceiver, intentToReceiveFilter);
		  isBatteryChangeReceiversRegistered = true;
    	
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
		if(isBatteryChangeReceiversRegistered) {
			unregisterReceiver(batteryChangedIntentReceiver);
			isBatteryChangeReceiversRegistered = false;
		}
    	super.onPause();
    }
    
}
