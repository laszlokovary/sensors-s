package com.lkovari.mobile.apps.sensors;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

 public class Main extends Activity {
	public static String BUNDLE_CONTENT_TAG_DATA1 = "D1";	
	public static String BUNDLE_CONTENT_TAG_DATA2 = "D2";	
	public static String BUNDLE_CONTENT_TAG_DATA3 = "D2";
	
	
	private SensorManager sensorManager = null;
	private Context context;
    private Activity sensorActivity;

    private int[] sensorTypes = new int[] {Sensor.TYPE_ACCELEROMETER, 
    									  //#3
    									  Sensor.TYPE_MAGNETIC_FIELD, 
    									  //#3
    									  Sensor.TYPE_ORIENTATION, 
    									  //#3
    									  Sensor.TYPE_GYROSCOPE,
    									  //#3
    									  Sensor.TYPE_LIGHT,
    									  //#3
    									  Sensor.TYPE_PRESSURE,
    									  //#3
    									  Sensor.TYPE_TEMPERATURE,
    									  //#3
    									  Sensor.TYPE_PROXIMITY,
    									  //#9
    									  Sensor.TYPE_GRAVITY,
    									  //#9
    									  Sensor.TYPE_LINEAR_ACCELERATION,
    									  //#9
    									  Sensor.TYPE_ROTATION_VECTOR,
    									  //#14
    									  Sensor.TYPE_RELATIVE_HUMIDITY,
    									  //#14
    									  Sensor.TYPE_AMBIENT_TEMPERATURE,
    									  //#18
    									  Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED,
    									  //#18
    									  Sensor.TYPE_GAME_ROTATION_VECTOR,
    									  //#18
    									  Sensor.TYPE_GYROSCOPE_UNCALIBRATED,
    									  //#18
    									  Sensor.TYPE_SIGNIFICANT_MOTION,
    									  //#19
    									  Sensor.TYPE_STEP_DETECTOR,    									  
    									  //#19
    									  Sensor.TYPE_STEP_COUNTER,
    									  //#19
    									  Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR,
    									  //#20
    									  Sensor.TYPE_HEART_RATE
    									  };
    
    TextView authorNameTextView = null;
    TextView authorTextView = null;
    
    ListView sensorListView = null;
    
    private SensorListener sensorEventListener = new SensorListener();

//	Sensor sensor = null;
    
	private ArrayList<SensorDescriptor> sensorDescriptorList = new ArrayList<SensorDescriptor>();
	
	private int numberOfAllSensors = 0;
	private int numberOfAvailableSensors = 0;
	
	private int selectedType = -1;

	private SensorsShowOptionKind sensorsShowOptionKind = SensorSettings.SENSOR_SHOW_OPTION_KIND;
	
	private TextView deviceNameTextView = null;
	
	private TriggerListener triggerListener = null;
	private String availableTitle = null;
	
	@SuppressLint("NewApi")
	class TriggerListener extends TriggerEventListener {
		@Override
		public void onTrigger(TriggerEvent event) {
			
		}
	}	
	
	class SensorUncaughtExceptionHandler implements UncaughtExceptionHandler {
		private UncaughtExceptionHandler defaultUEH;
		private Exception throwable = null;
		private Context context;
		
		
		public SensorUncaughtExceptionHandler(Context context) {
			this.context = context;
	        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		}
		
		
		public Context getContext() {
			return context;
		}

		
	    public void uncaughtException(Thread t, Throwable ex) {
	        final Writer result = new StringWriter();
	        final PrintWriter printWriter = new PrintWriter(result);
	        throwable.printStackTrace(printWriter);
	        String stacktrace = result.toString();
	        printWriter.close();
	        Toast.makeText(context, stacktrace, Toast.LENGTH_LONG).show();
        	SDCardManager.createErrorLog(context, throwable);
	        defaultUEH.uncaughtException(t, ex);
	    }

	
	}
	
	
	private void passCommonInfo(Activity currentContent, Intent moduleIntent, Long data1, Long data2, Long data3) {
		Bundle b = new Bundle();
		b.putLong(BUNDLE_CONTENT_TAG_DATA1, data1);
		b.putLong(BUNDLE_CONTENT_TAG_DATA2, data2);
		b.putLong(BUNDLE_CONTENT_TAG_DATA3, data3);
		moduleIntent.putExtras(b);
	}

	private String extractSensorUnregMess(int type) {
		String sensorUnregMess = "";
		if (type == Sensor.TYPE_ACCELEROMETER) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_accelerometer_sensor_listener);
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_ambienttemperature_sensor_listener);
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gravity_sensor_listener);
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gyroscope_sensor_listener);
		}
		else if (type == Sensor.TYPE_LIGHT) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_light_sensor_listener);
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_linearacceleration_sensor_listener);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_magneticfield_sensor_listener);
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_orientation_sensor_listener);
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_pressure_sensor_listener);
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_proximity_sensor_listener);
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_relativehumidity_sensor_listener);
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_rotationvector_sensor_listener);
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_temperature_sensor_listener);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_magneticfieldu_sensor_listener);
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_geomagrotationvector_sensor_listener);
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gamerotationvector_sensor_listener);
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gyroscopeu_sensor_listener);
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_significantmotion_sensor_listener);
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_stepcounter_sensor_listener);
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_stepdetector_sensor_listener);
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_heartrate_sensor_listener);
		}
		else {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_sensor_listener);
		}
		return sensorUnregMess;
	}
	
	private boolean isSensorStandard(int type) {
		boolean isSensorTypeExists = false;
		for (int sensorType : sensorTypes) {
			if (sensorType == type) {
				isSensorTypeExists = true;
				break;
			}
		}
		return isSensorTypeExists;
	}
	
    @SuppressLint("NewApi") 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		

        this.context = getApplicationContext();
        this.sensorActivity = this;

		Thread.setDefaultUncaughtExceptionHandler(new SensorUncaughtExceptionHandler(this.context));
        
		
		deviceNameTextView = (TextView)findViewById(R.id.deviceNameTextView);
		if (deviceNameTextView != null)
			deviceNameTextView.setText(extractDeviceName(false));

		// 2015.08.15.
		SensorSettings.loadPreferences(getApplicationContext());
		
		// Set Sensor Manager
        this.sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        
        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        
        if (android.os.Build.VERSION.SDK_INT >= 18) {
        	this.triggerListener = new TriggerListener();
        }	
        
        this.sensorDescriptorList.clear();

    	Sensor msensor = null;

        if (this.sensorTypes.length >= sensorList.size()) {
            this.numberOfAllSensors = this.sensorTypes.length;
            for (int type : this.sensorTypes) {
            	String text = SensorUtils.extractSensorName(getResources(), type);
            	String doesNotExists = getResources().getString(R.string.doesnot_exists_text);
            	msensor = sensorManager.getDefaultSensor(type);
            	SensorDescriptor sensorDescriptor = new SensorDescriptor();
            	boolean isSDKVersionOK = false;
            	switch (type) {
            	case Sensor.TYPE_GYROSCOPE : 
            	case Sensor.TYPE_PRESSURE : 
            	case Sensor.TYPE_GRAVITY : 
            	case Sensor.TYPE_LINEAR_ACCELERATION :
            	case Sensor.TYPE_ROTATION_VECTOR : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 9);
            		break;
            	}
            	case Sensor.TYPE_RELATIVE_HUMIDITY :
            	case Sensor.TYPE_AMBIENT_TEMPERATURE : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 14);
            		break;
            	}
            	case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
            		break;
            	}
            	case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 19);
            		break;
            	}
            	case Sensor.TYPE_GAME_ROTATION_VECTOR : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
            		break;
            	}
            	case Sensor.TYPE_GYROSCOPE_UNCALIBRATED : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
            		break;
            	}
            	case Sensor.TYPE_SIGNIFICANT_MOTION : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
            		break;
            	}
            	case Sensor.TYPE_STEP_COUNTER : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 19);
            		break;
            	}
            	case Sensor.TYPE_STEP_DETECTOR : {
            		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 19);
            		break;
            	}
            	default : {
            		isSDKVersionOK = true;
            	}
            	}
            	if (isSDKVersionOK) {
                	if (msensor != null) {
                    	String sensorTitle = SensorUtils.constructSensorTitle(text, msensor, type);
                    	String sensorName = SensorUtils.constructSensorName(msensor);
                    	String sensorDetails = SensorUtils.constructSensorDetails(msensor);
                    	sensorDescriptor.setImageNumber(0);
                    	sensorDescriptor.setTitle(sensorTitle);
                    	sensorDescriptor.setName(sensorName);
                    	sensorDescriptor.setDetails(sensorDetails);
                    	sensorDescriptor.setType(type);
                    	boolean isSuccess = false;
                    	if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
                    		if (android.os.Build.VERSION.SDK_INT >= 18) {
                    			if (triggerListener != null) {
                    				isSuccess = sensorManager.requestTriggerSensor(triggerListener, msensor);
                    			}
                    		} 
                    	}
                    	else {
                    		isSuccess = sensorManager.registerListener(sensorEventListener, msensor, SensorManager.SENSOR_DELAY_NORMAL);
                    	}	
                    	if (!isSuccess) { 
                    		sensorDescriptor.setImageNumber(1);
                    		sensorDescriptor.setTitle("" + type + " " + text);
                    		sensorDescriptor.setName(extractSensorUnregMess(type));
                    		sensorDescriptor.setDetails("");
                        	sensorDescriptor.setAvailable(false);
                    	}	
                    	else {
                        	sensorDescriptor.setAvailable(true);
                    	}
                	}
                	else {
                		sensorDescriptor.setImageNumber(1);
                		sensorDescriptor.setTitle("" + type + " " + text);
                		sensorDescriptor.setName(doesNotExists);
                		sensorDescriptor.setDetails("");
                    	sensorDescriptor.setAvailable(false);
                	}
            	}
            	else {
        			sensorDescriptor.setImageNumber(1);
        			sensorDescriptor.setTitle("" + type + " " + text);
        			sensorDescriptor.setName(doesNotExists);
        			sensorDescriptor.setDetails("");
        			sensorDescriptor.setAvailable(false);
            	}
            	
            	switch (sensorsShowOptionKind) {
            	case ALL_SENSORS : {
                   	this.sensorDescriptorList.add(sensorDescriptor);
                   	break;
            	}
            	case AVAILABLE_SENSORS : {
        			if (sensorDescriptor.isAvailable()) {
                    	this.sensorDescriptorList.add(sensorDescriptor);
        			}
        			break;
            	}
            	}
            }
            
        }
        else {
            this.numberOfAllSensors = sensorList.size();
            for (Sensor sensor : sensorList) {
            	if (sensor != null) {
                	int type = sensor.getType();
                	String text = SensorUtils.extractSensorName(getResources(), sensor.getType());
                	String doesNotExists = getResources().getString(R.string.doesnot_exists_text);
                	SensorDescriptor sensorDescriptor = new SensorDescriptor();
                	boolean isSDKVersionOK = false;
                	switch (type) {
                	case Sensor.TYPE_GYROSCOPE : 
                	case Sensor.TYPE_PRESSURE : 
                	case Sensor.TYPE_GRAVITY : 
                	case Sensor.TYPE_LINEAR_ACCELERATION :
                	case Sensor.TYPE_ROTATION_VECTOR : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 9);
                		break;
                	}
                	case Sensor.TYPE_RELATIVE_HUMIDITY :
                	case Sensor.TYPE_AMBIENT_TEMPERATURE : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 14);
                		break;
                	}
                	case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
                		break;
                	}
                	case Sensor.TYPE_GAME_ROTATION_VECTOR : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
                		break;
                	}
                	case Sensor.TYPE_GYROSCOPE_UNCALIBRATED : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
                		break;
                	}
                	case Sensor.TYPE_SIGNIFICANT_MOTION : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 18);
                		break;
                	}
                	case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 19);
                		break;
                	}
                	case Sensor.TYPE_STEP_COUNTER : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 19);
                		break;
                	}
                	case Sensor.TYPE_STEP_DETECTOR : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 19);
                		break;
                	}
                	case Sensor.TYPE_HEART_RATE : {
                		isSDKVersionOK = (android.os.Build.VERSION.SDK_INT >= 20);
                		break;
                	}
                	default : {
                		isSDKVersionOK = true;
                	}
                	}
                	if (isSDKVersionOK) {
                    	if (sensor != null) {
                        	String sensorTitle = SensorUtils.constructSensorTitle(text, sensor, type);
                        	String sensorName = SensorUtils.constructSensorName(sensor);
                        	String sensorDetails = SensorUtils.constructSensorDetails(sensor);
                        	sensorDescriptor.setImageNumber(0);
                        	sensorDescriptor.setTitle(sensorTitle);
                        	sensorDescriptor.setName(sensorName);
                        	sensorDescriptor.setDetails(sensorDetails);
                        	sensorDescriptor.setType(type);
                        	boolean isSuccess = false;
                        	if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
                        		if (android.os.Build.VERSION.SDK_INT >= 18) {
                        			if (triggerListener != null) {
                        				isSuccess = sensorManager.requestTriggerSensor(triggerListener, sensor);
                        			}
                        		} 
                        	}
                        	else {
                        		isSuccess = sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
                        	}	
                        	if (!isSuccess) { 
                        		sensorDescriptor.setImageNumber(1);
                        		sensorDescriptor.setTitle("" + type + " " + text);
                        		sensorDescriptor.setName(extractSensorUnregMess(type));
                        		sensorDescriptor.setDetails("");
                            	sensorDescriptor.setAvailable(false);
                        	}	
                        	else {
                            	sensorDescriptor.setAvailable(isSensorStandard(type));
                        	}
                    	}
                    	else {
                    		sensorDescriptor.setImageNumber(1);
                    		sensorDescriptor.setTitle("" + type + " " + text);
                    		sensorDescriptor.setName(doesNotExists);
                    		sensorDescriptor.setDetails("");
                        	sensorDescriptor.setAvailable(false);
                    	}
                	}
                	else {
            			sensorDescriptor.setImageNumber(1);
            			sensorDescriptor.setTitle("" + type + " " + text);
            			sensorDescriptor.setName(doesNotExists);
            			sensorDescriptor.setDetails("");
            			sensorDescriptor.setAvailable(false);
                	}
                	
                	switch (sensorsShowOptionKind) {
                	case ALL_SENSORS : {
                       	this.sensorDescriptorList.add(sensorDescriptor);
                       	break;
                	}
                	case AVAILABLE_SENSORS : {
            			if (sensorDescriptor.isAvailable()) {
                        	this.sensorDescriptorList.add(sensorDescriptor);
            			}
            			break;
                	}
                	}
                }
                
            }	
        }
        
        // check
        this.numberOfAvailableSensors = 0;
        // check sensors
       	for (SensorDescriptor sensorDescriptor : this.sensorDescriptorList) {
   			if (sensorDescriptor.isAvailable()) {
   				this.numberOfAvailableSensors++;
   			}
        }
        
        // change title
        String title = (String) this.getTitle();
        availableTitle = title + " #" + this.numberOfAvailableSensors + "/" + this.numberOfAllSensors;
        this.setTitle(availableTitle);
        
        // unregister sensors
        for (Sensor sensor : sensorList) {
        	sensorManager.unregisterListener(sensorEventListener, sensor);
        }
        
        sensorList = null;
        msensor = null;
        sensorManager = null;
        
     
        final ListView sensorsListView = (ListView) findViewById(R.id.SensorsListView);
        sensorsListView.setAdapter(new SensorsListViewAdapter(this, sensorDescriptorList));
        
        sensorsListView.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
        		Object o = sensorsListView.getItemAtPosition(position);
        		if (o != null) {
        			SensorDescriptor sensorDescriptor = (SensorDescriptor)o;
        			if (sensorDescriptor.getImageNumber() == 0) {
        				selectedType = sensorDescriptor.getType();
        				// start the Map Selector
        		        Intent intent = new Intent();
        		        intent.setClass(context, SensorValues.class);
        		        // start with download map mode
        	        	passCommonInfo(sensorActivity, intent, new Long(selectedType), new Long(0), new Long(0));
        		        startActivity(intent);      
        			}	
        		}
        	}  
        });
    	
    }
	
		
	class SensorListener implements SensorEventListener {
		public void onSensorChanged(SensorEvent event) {
        }

		@Override
		public void onAccuracyChanged(Sensor sensor, int value) {
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
	}
    
    @Override
    protected void onResume() {
        super.onResume();
    }
 
    @Override
    protected void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
    }
    
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.optionsmenu, menu);
		return true;
	}
    
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent();
		
		switch (item.getItemId()) {
		case R.id.itemAbout:
	        intent = new Intent();
	        intent.setClass(getApplicationContext(), Info.class);
	        startActivity(intent);      
			return true;
		case R.id.sensor_itemSettings:
			intent = new Intent();
			intent.setClass(getApplicationContext(), SensorPreferenceSettings.class);
			startActivity(intent);      
			return true;
		case R.id.sensor_itemSave :
			if (SDCardManager.isStoreDebugLog) {
				SDCardManager.createFileForStringBuffer(this.getApplicationContext(), "Sensor_log.txt", SDCardManager.log);
				SDCardManager.log.delete(0, SDCardManager.log.length());
			}	
			saveSensorList();
			break;
		}
		return true;
	}
 
	
	 @SuppressLint("NewApi") 
	 public String extractDeviceName(boolean withSerial) {
		String deviceName = "";
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			deviceName = deviceNameToCapitalize(model);
		} 
		else {
			deviceName = deviceNameToCapitalize(manufacturer) + " " + model;
		}
		if (withSerial) {
			if (android.os.Build.VERSION.SDK_INT >= 9) {
				deviceName += " (sn:" +Build.SERIAL+")";
			}	
		}
		return deviceName;
	 }


	 private String deviceNameToCapitalize(String s) {
	   if (s == null || s.length() == 0) {
	     return "";
	   }
	   char first = s.charAt(0);
	   if (Character.isUpperCase(first)) {
	     return s;
	   } 
	   else {
	     return Character.toUpperCase(first) + s.substring(1);
	   }
	 } 

	 private String extractAndroidVersion() {
		 String ver = null;
		 StringBuilder builder = new StringBuilder();
		 builder.append("Android ").append(Build.VERSION.RELEASE);

		 Field[] fields = Build.VERSION_CODES.class.getFields();
		 for (Field field : fields) {
		     String fieldName = field.getName();
		     int fieldValue = -1;

		     try {
		         fieldValue = field.getInt(new Object());
		     } 
		     catch (IllegalArgumentException e) {
		         e.printStackTrace();
		     } 
		     catch (IllegalAccessException e) {
		         e.printStackTrace();
		     } 
		     catch (NullPointerException e) {
		         e.printStackTrace();
		     }

		     if (fieldValue == Build.VERSION.SDK_INT) {
		         builder.append(" ");
		         builder.append(fieldName);
		         builder.append(" ");
		         builder.append("SDK#").append(fieldValue);
		     }
		 }

		 ver = builder.toString();
		 builder = null;
		 return ver;
	 }
	 
	 private void saveSensorList() {
		 String fileName = "Sensors-" + Build.MODEL + ".txt";
		 StringBuffer list = new StringBuffer();
		 // device name and how many sensors are available
		 if (availableTitle != null)
			 list.append(extractDeviceName(true) + " built-in sensors (" + availableTitle + ") \n\r ");
		 else
			 list.append(extractDeviceName(true) + " built in sensors \n\r ");
		 // android version
		 String androidVer = extractAndroidVersion();
		 if (!androidVer.equals(""))
			 list.append(extractAndroidVersion() + " \n\r ");
		 list.append(" \n\r ");
		 // sensor availability
		 for (SensorDescriptor sensorDescriptor : sensorDescriptorList) {
			 String line = (sensorDescriptor.isAvailable()) ? "√ " : "X ";
			 line +=  sensorDescriptor.getTitle() + " : " + sensorDescriptor.getName() + "\n\r " +  ((sensorDescriptor.getDetails().trim() == "") ? "" : sensorDescriptor.getDetails() + "\n\r "); 
			 list.append(line);
		 }
		 list.append(" \n\r ");
		 // apk version and code
		 String ver = SensorUtils.extractVersionCodeAndName(this);
		 list.append("by Sensors v" + ver + " \n\r ");
		 list.append(getResources().getString(R.string.appmain_copyright) + "\n\r ");
		 list.append(" \n\r ");
		 String savePath = SDCardManager.createFileForStringBuffer(context, fileName, list);
		 if (savePath != null) {
			 Toast.makeText(context, savePath, Toast.LENGTH_LONG).show();
		 }
		 list = null;
	 }
	 
}