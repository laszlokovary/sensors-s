package com.lkovari.mobile.apps.sensors;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * 
 * @author lkovari
 *
 */
public class SensorPreferenceSettings extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	private boolean isChanged = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.sensor_preferences, false);
		
		SharedPreferences preferencesSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		preferencesSettings.registerOnSharedPreferenceChangeListener(this);

		addPreferencesFromResource(R.xml.sensor_preferences);
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals("key_sensorshowoptionkind")) {
			// 12/15/2012
			String value = sharedPreferences.getString("key_sensorshowoptionkind", "0");
			if (value.equals("0")) {
				SensorSettings.SENSOR_SHOW_OPTION_KIND = SensorsShowOptionKind.ALL_SENSORS;
			}
			else if (value.equals("1")) {
				SensorSettings.SENSOR_SHOW_OPTION_KIND = SensorsShowOptionKind.AVAILABLE_SENSORS;
			}
			isChanged = true;
		}
		else if (key.equals("key_qnh_value")) {
			String defaultValue = "" + SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH;
			String qnhValue = sharedPreferences.getString("key_qnh_value", defaultValue);
			double qnh_value = SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH;
			try {
				if (SensorSettings.isNumeric(getApplicationContext(), qnhValue)) {
					qnh_value = Double.parseDouble(qnhValue);
				}
				else {
					Toast.makeText(getApplicationContext(), R.string.error_qnh_value_invalid, Toast.LENGTH_LONG).show();
				}
			}
			catch (Exception e) {
				Toast.makeText(getApplicationContext(), R.string.error_qnh_value_invalid, Toast.LENGTH_LONG).show();
			}
			SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH = qnh_value;
			isChanged = true;
		}

	}

	@Override
	protected void onStop() {
		// #5 10/07/2013
		if (isChanged) {
			Toast.makeText(SensorPreferenceSettings.this, R.string.settings_take_effect, Toast.LENGTH_LONG).show();
		}	
		super.onStop();
	}
	
}
