package com.lkovari.mobile.apps.sensors;

import java.io.Serializable;


public class SensorDescriptor implements Serializable {
	private static final long serialVersionUID = -4327199377240961483L;
	private String title = null;
	private String name = null;
	private String details = null;
	private int imageNumber = 0;
	private int type = -1;
	private boolean isAvailable = false;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}
	
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public void setImageNumber(int imageNumber) {
		this.imageNumber = imageNumber;
	}
	public int getImageNumber() {
		return imageNumber;
	}
}
