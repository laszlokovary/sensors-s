package com.lkovari.mobile.apps.sensors;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.hardware.Sensor;

/**
 * 
 * @author lkovari
 *
 */
public class SensorUtils {

	public static String extractSensorName(Resources resources, int type) {
		String sensorName = "";
		if (type == Sensor.TYPE_ACCELEROMETER) {
			sensorName = resources.getString(R.string.accelerometer_text_title);
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			sensorName = resources.getString(R.string.ambienttemperature_text_title);
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			sensorName = resources.getString(R.string.gravity_text_title);
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			sensorName = resources.getString(R.string.gyroscope_text_title);
		}
		else if (type == Sensor.TYPE_LIGHT) {
			sensorName = resources.getString(R.string.light_text_title);
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			sensorName = resources.getString(R.string.linearacceleration_text_title);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			sensorName = resources.getString(R.string.magneticfield_text_title);
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			sensorName = resources.getString(R.string.orientation_text_title);
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			sensorName = resources.getString(R.string.presure_text_title);
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			sensorName = resources.getString(R.string.presure_text_title);
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			sensorName = resources.getString(R.string.proximity_text_title);
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			sensorName = resources.getString(R.string.relativehumidity_text_title);
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			sensorName = resources.getString(R.string.rotationvector_text_title);
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			sensorName = resources.getString(R.string.temperature_text_title);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			sensorName = resources.getString(R.string.uncalibratedmagneticfield_text_title);
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			sensorName = resources.getString(R.string.geomagrotationvector_text_title);
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			sensorName = resources.getString(R.string.gamerotationvector_text_title);
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			sensorName = resources.getString(R.string.uncalibrategyroscope_text_title);
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			sensorName = resources.getString(R.string.significantmotion_text_title);
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			sensorName = resources.getString(R.string.stepcounter_text_title);
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			sensorName = resources.getString(R.string.stepdetector_text_title);
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			sensorName = resources.getString(R.string.heartrate_text_title);
		}
		else {
			sensorName = resources.getString(R.string.sensor_unknown_text_title);
		}
		return sensorName;
	}
	
	public static String constructSensorTitle(String text, Sensor sensor, int type) {
		String sensorTitle = "" + type + " " + text;
		return sensorTitle;
	}
	
	
	public static String constructSensorName(Sensor sensor) {
		String sensorName = "";
		if (sensor != null) {
			sensorName += sensor.getName() + " v" + sensor.getVersion() + " by:" + sensor.getVendor();
		}
		return sensorName;
	}

	@SuppressLint("NewApi") 
	public static String constructSensorDetails(Sensor sensor) {
		String sensorDetails = "";
		if (sensor != null) {
	        if (android.os.Build.VERSION.SDK_INT >= 9) {
	        	sensorDetails += "Pwr " + sensor.getPower() + "mA MinDly " + sensor.getMinDelay() + " Res " + sensor.getResolution() + " MaxRng " + sensor.getMaximumRange();
	        }
	        else {
	        	sensorDetails += "Pwr " + sensor.getPower() + "mA Res " + sensor.getResolution() + " MaxRng " + sensor.getMaximumRange();
	        }
		}
		return sensorDetails;
	}
	
	/**
	 * 02/09/2014
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	public static String extractVersionCodeAndName(Context ctx) {
		String ver = "";
		PackageInfo pinfo = null;
		try {
			pinfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
			if (pinfo != null)
				ver = pinfo.versionName + " #" + pinfo.versionCode;
		} 
		catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return ver;
	}
}
