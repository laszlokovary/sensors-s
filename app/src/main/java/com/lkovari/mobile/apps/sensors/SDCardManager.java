package com.lkovari.mobile.apps.sensors;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

public class SDCardManager {
	private static String fileExt = null;
	private static final int TIMEOUT_CONNECTION = 5000;
	private static final int TIMEOUT_SOCKET = 30000;
    private static int LOG_FILE_SIZE_LIMIT = 100 * 1024;
	public static String BASE_FILE_NAME = "Sensors-";
	public static String FILE_STAMP = null;

	
	public static StringBuffer log = new StringBuffer();
	private static int cnt = 0;
	public static boolean isStoreDebugLog = false;
	
	public static void debug(String text) {
		if (isStoreDebugLog) {
			cnt++;
			log.append("> " + cnt + ". " + text + "\n");
		}
	}
	
    public static String calculateFileNameStamp() {
    	String fileStamp = null;
       	Calendar c = Calendar.getInstance();
    	if (FILE_STAMP == null) {
           	FILE_STAMP = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.DAY_OF_YEAR) + "-" + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE) + c.get(Calendar.SECOND) + c.get(Calendar.MILLISECOND);
           	fileStamp = FILE_STAMP;
    	}
    	else {
    		fileStamp = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.DAY_OF_YEAR) + "-" + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE) + c.get(Calendar.SECOND) + c.get(Calendar.MILLISECOND);
    	}
    	c = null;
       	return fileStamp;
    }
	
	/**
	 * 
	 * @param e
	 * @return
	 * @throws Exception 
	 */
	public synchronized static void createErrorLog(Context context, Exception e) {
		StringBuffer errorLog = new StringBuffer();
		errorLog.append(e.getMessage() + "\r\n");
		StackTraceElement[] trace = e.getStackTrace();
		if (trace != null) {
			for (int ix = 0; ix < trace.length; ix++) {
				StackTraceElement ste = trace[ix];
				String line = ste.getFileName() + " " + ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber()+ "\r\n"; 
				errorLog.append(line); 
			}
			Date time = new Date();
        	String timeStamp = calculateFileNameStamp();
			String fileName = BASE_FILE_NAME + "E-" + timeStamp + ".txt";
			if (context != null) {
				SDCardManager.createFileForStringBuffer(context, fileName, errorLog);
			}	
			errorLog = null;
		}
	}

	
	/**
	 * #6 11/26/2013
	 * @param context
	 * @param state
	 * @param isShowMessage
	 */
	public static void checkMediaState(Context context, String state, boolean isShowMessage) {
		String mess = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
        	// nothing to do
        } 
        else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
    		if (isShowMessage)
    			mess =  context.getResources().getString(R.string.error_externalstorage_mounted_read_only);;
        }
        else if (Environment.MEDIA_REMOVED.equals(state)) {
    		if (isShowMessage)
    			mess = context.getResources().getString(R.string.error_externalstorage_not_present);
        } 
        else if (Environment.MEDIA_UNMOUNTABLE.equals(state)){
    		if (isShowMessage)
    			mess = context.getResources().getString(R.string.error_externalstorage_media_unmountable);
        }	    
        else if (Environment.MEDIA_BAD_REMOVAL.equals(state)){
        	mess = context.getResources().getString(R.string.error_externalstorage_media_bad_removal);
        }
        else if (Environment.MEDIA_NOFS.equals(state)){
        	mess = context.getResources().getString(R.string.error_externalstorage_nofs);
        }
        else if (Environment.MEDIA_UNMOUNTED.equals(state)){
        	mess = context.getResources().getString(R.string.error_externalstorage_media_unmounted);
        }
		if (isShowMessage && (mess != null))
			Toast.makeText(context, mess, Toast.LENGTH_LONG).show();
	}
	
	/**
	 * #5 10/06/2013
	 * @param context
	 * @param isShowMessage
	 * @return
	 */
	public static String checkExternalStorageAvailable(Context context, boolean isShowMessage) {
		String mess = null;
		if (context == null)
			isShowMessage = false;
        String state = Environment.getExternalStorageState();
        checkMediaState(context, state, isShowMessage);
	    return state;
	}	
	
	/**
	 * 01/10/2013 
	 * @param urlText
	 * @return
	 * @throws Exception
	public static File downloadFileToSDCard(ProgressDialog progressBar, String urlText, String folder) throws Exception {
		File file = null;
		// #5 10/01/2013 
		String state = checkExternalStorageAvailable(progressBar.getContext(), true);
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    File root = Environment.getExternalStorageDirectory();
		    if (root.canWrite()) {

		        String fileName = null;
		        if (urlText.endsWith(".map")) {
		        	
		        	String[] fragments = urlText.split("/");
		        	fileName = fragments[fragments.length - 1];
			        file = new File(root.getPath() + "/" + folder + "/" + fileName);

			        URL url = new URL(urlText);

			        //Open a connection to that URL.
			        URLConnection urlConnection = url.openConnection();

			        int fileSize = 0;
			        if (progressBar != null) {
				        urlConnection.connect();
				        fileSize = urlConnection.getContentLength();
			        }
			        
			        //this timeout affects how long it takes for the app to realize there's a connection problem
			        urlConnection.setReadTimeout(TIMEOUT_CONNECTION);
			        urlConnection.setConnectTimeout(TIMEOUT_SOCKET);


			        //Define InputStreams to read from the URLConnection.
			        // uses 3KB download buffer
			        InputStream is = urlConnection.getInputStream();
			        BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
			        FileOutputStream outStream = new FileOutputStream(file);
			        byte[] buff = new byte[5 * 1024];

			        if (progressBar != null) {
						progressBar.setProgress(0);
						progressBar.setMax(fileSize / 1014);		        
			        }
					
			        //Read bytes (and store them) until there is nothing more to read(-1)
			        int len;
			        while ((len = inStream.read(buff)) != -1)  {
			            outStream.write(buff,0,len);
				        if (progressBar != null) {
				        	progressBar.setProgress(len / 1024);
				        }	
			        }

			        //clean up
			        outStream.flush();
			        outStream.close();
			        inStream.close();
		        }
		    }
		}
		return file;
	}
	*/
	
	/**
	 * 12/30/2012
	 * @return
	 */
	public static boolean isMapFileExists() {
		boolean isMapFileExists = false;
		// #5 10/06/2013 
		String state = checkExternalStorageAvailable(null, false);
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			// #5 10/06/2013
			List<File> foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/sensors/", ".map");
			if (foundMapFiles.size() > 0) {
				// first map file
				File mapFile = foundMapFiles.get(0);
				isMapFileExists = (mapFile != null) && mapFile.exists();
				mapFile = null;
			}
			foundMapFiles.clear();
			foundMapFiles = null;
		}
		return isMapFileExists;
	}
	
	/**
	 * 12/30/2012
	 * @param folderName
	 * @return
	 */
	public static boolean createFolderOnSDCard(String folderName) {
		boolean isSuccess = false;
		try {
			// #5 10/06/2013
			String state = checkExternalStorageAvailable(null, false);
			if (state.equals(Environment.MEDIA_MOUNTED)) {
			    File root = Environment.getExternalStorageDirectory();
			    if (root.canWrite()) {
					File folder = new File(root.getPath() + "/"  + folderName + "/");
					if (!folder.exists()) {
						isSuccess = folder.mkdirs();	
					}	
					else {
						isSuccess = true;
					}	
			    }
			}
		}
		catch (Exception e) {
			isSuccess = false;
		}
		return isSuccess;
	}
	
	/**
	 * #5 10/06/2013
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public static void copyFile(File src, File dst) throws IOException {
    	BufferedInputStream buffInStream = null;
    	try {
    		buffInStream = new BufferedInputStream(new FileInputStream(src));
    	    BufferedOutputStream buffOutStream = null;
    	    try {
    	    	buffOutStream = new BufferedOutputStream(new FileOutputStream(dst, false));
    	        byte[] buffer = new byte[1024];
    	        int len;
    	        while ((len = buffInStream.read(buffer)) > 0) {
    	        	buffOutStream.write(buffer, 0, len);
    	        }
    	    }
    	    finally {
    	        if (buffOutStream != null) { 
    	        	buffOutStream.close();
    	        }	
    	    }
    	} finally {
    	    if (buffInStream != null) {
    	    	buffInStream.close();
    	    }	
    	}	    	
	}
	
	/**
	 * 12/29/2012
	 * @param directory
	 * @param ext
	 * @return
	 */
	public static List<File> findFiles(String directory, String ext) {
        List<File> foundFiles = new ArrayList<File>();
        fileExt = ext;
        File dir = new File(directory);
        File[] fileMatches = dir.listFiles(new FilenameFilter() {
          public boolean accept(File dir, String name)  {
             return name.endsWith(fileExt);
          }
        });
        // add to list
        if (fileMatches != null) {
            for (File f : fileMatches) {
                foundFiles.add(f);
            }
            // order
            Collections.sort(foundFiles, new Comparator<File>() {
                @Override
                public int compare(File lhs, File rhs) {
                    int res = lhs.getName().compareTo(rhs.getName());
                    return res;
                }
                
            });
        }
        return foundFiles;
	}

	/**
	 * 01/13/2013
	 * @param path
	 * @return
	 */
	public static List<File> findFile(String path) {
        List<File> foundFiles = new ArrayList<File>();
        File file = new File(path);
        if (file.exists()) {
        	foundFiles.add(file);
        }
        return foundFiles;
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static boolean isFileExists(String fileName) {
		boolean isFileExists = false;
		// #5 10/01/2013
		String state = checkExternalStorageAvailable(null,false);
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    File root = Environment.getExternalStorageDirectory();
		    if (root.canRead()) {
		        File file = new File(root, fileName);
		        isFileExists = file.exists();
		        file = null;
		    }
		}
	    return isFileExists;
	}
	
	/**
	 * 10/29/2012
	 * @param context
	 * @param fileName
	 * @param content
	 */
	public static String createFileForStringBuffer(Context context, String fileName, StringBuffer content) {
		// 11/25/2012
		String filePath = null;
		try {
			// #5 10/01/2013
			String state = checkExternalStorageAvailable(context, true);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				File root = Environment.getExternalStorageDirectory();
			    if (root.canWrite()) {
			    	// #5 10/06/2013
			        File file = null;
			    	File folderDir = new File(Environment.getExternalStorageDirectory() + "/sensors");
			    	if (!folderDir.exists()) {
			    		if (folderDir.mkdir()) {
					        file = new File(folderDir.getAbsolutePath() + "/" + fileName);
			    		}
			    		else {
					        throw new Exception("Directory can\'t created " + folderDir.getAbsolutePath());
			    		}
			    	}
			    	else {
				        file = new File(folderDir.getAbsolutePath() + "/" + fileName);
			    	}
			        FileWriter fileWriter = new FileWriter(file);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(content.toString());
			        bufferWriter.flush();
			        bufferWriter.close();
					// 11/25/2012
			        filePath = file.getAbsolutePath();
			    }
				else {
					Toast.makeText(context, "ERROR! SDCard media is read only!", Toast.LENGTH_LONG).show();
				}
			}
			else {
				Toast.makeText(context, "ERROR! SDCard media is not mounted!", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
    		e.printStackTrace();
			Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
			// 11/25/2012
			filePath = null;
		}	
		// 11/25/2012
		return filePath;
	}

	/**
	 * 10/29/2012
	 * @param context
	 * @param fileName
	 * @param content
	 */
	public static void createFileForString(Context context, String fileName, String content) {
		File storage = null;
		try {
			// #5 10/01/2013
			String state = checkExternalStorageAvailable(context, true);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				storage = Environment.getExternalStorageDirectory();
			    if (storage.canWrite()){
			        File file = new File(storage, fileName);
			        FileWriter fileWriter = new FileWriter(file);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(content);
			        bufferWriter.flush();
			        bufferWriter.close();
			    }
			}
			else {
				// #5 10/06/2013 try to use internal storage
				storage = context.getFilesDir();
				if (storage.canWrite()) {
			        File file = new File(storage, fileName);
			        FileWriter fileWriter = new FileWriter(file);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(content);
			        bufferWriter.flush();
			        bufferWriter.close();
				}
			}
		} catch (IOException e) {
    		e.printStackTrace();
            if (context != null)
            	Toast.makeText(context, "ERROR when createFile " + e.getMessage() + "\n" + storage.getAbsolutePath(), Toast.LENGTH_LONG).show();
		}		
	}
	}
