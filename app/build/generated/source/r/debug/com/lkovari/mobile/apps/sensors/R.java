/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.lkovari.mobile.apps.sensors;

public final class R {
    public static final class array {
        public static final int SensorShowOptionKindEntry=0x7f050000;
        public static final int SensorShowOptionKindValue=0x7f050001;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int AliceBlue=0x7f060000;
        public static final int AntiqueWhite=0x7f060001;
        public static final int Aqua=0x7f060002;
        public static final int Aquamarine=0x7f060003;
        public static final int Azure=0x7f060004;
        public static final int Beige=0x7f060005;
        public static final int Bisque=0x7f060006;
        public static final int Black=0x7f060007;
        public static final int BlanchedAlmond=0x7f060008;
        public static final int Blue=0x7f060009;
        public static final int BlueViolet=0x7f06000a;
        public static final int Brown=0x7f06000b;
        public static final int BurlyWood=0x7f06000c;
        public static final int CadetBlue=0x7f06000d;
        public static final int Chartreuse=0x7f06000e;
        public static final int Chocolate=0x7f06000f;
        public static final int Coral=0x7f060010;
        public static final int CornflowerBlue=0x7f060011;
        public static final int Cornsilk=0x7f060012;
        public static final int Crimson=0x7f060013;
        public static final int Cyan=0x7f060014;
        public static final int DarkBlue=0x7f060015;
        public static final int DarkCyan=0x7f060016;
        public static final int DarkGoldenrod=0x7f060017;
        public static final int DarkGray=0x7f060018;
        public static final int DarkGreen=0x7f060019;
        public static final int DarkKhaki=0x7f06001a;
        public static final int DarkMagenta=0x7f06001b;
        public static final int DarkOliveGreen=0x7f06001c;
        public static final int DarkOrange=0x7f06001d;
        public static final int DarkOrchid=0x7f06001e;
        public static final int DarkRed=0x7f06001f;
        public static final int DarkSalmon=0x7f060020;
        public static final int DarkSeaGreen=0x7f060021;
        public static final int DarkSlateBlue=0x7f060022;
        public static final int DarkSlateGray=0x7f060023;
        public static final int DarkTurquoise=0x7f060024;
        public static final int DarkViolet=0x7f060025;
        public static final int DeepPink=0x7f060026;
        public static final int DeepSkyBlue=0x7f060027;
        public static final int DimGray=0x7f060028;
        public static final int DodgerBlue=0x7f060029;
        public static final int FireBrick=0x7f06002a;
        public static final int FloralWhite=0x7f06002b;
        public static final int ForestGreen=0x7f06002c;
        public static final int Fuchsia=0x7f06002d;
        public static final int Gainsboro=0x7f06002e;
        public static final int GhostWhite=0x7f06002f;
        public static final int Gold=0x7f060030;
        public static final int Goldenrod=0x7f060031;
        public static final int Gray=0x7f060032;
        public static final int Green=0x7f060033;
        public static final int GreenYellow=0x7f060034;
        public static final int GtlLightPurple=0x7f060035;
        public static final int Honeydew=0x7f060036;
        public static final int HotPink=0x7f060037;
        public static final int IndianRed=0x7f060038;
        public static final int Indigo=0x7f060039;
        public static final int Ivory=0x7f06003a;
        public static final int Khaki=0x7f06003b;
        public static final int Lavender=0x7f06003c;
        public static final int LavenderBlush=0x7f06003d;
        public static final int LawnGreen=0x7f06003e;
        public static final int LemonChiffon=0x7f06003f;
        public static final int LightBlue=0x7f060040;
        public static final int LightCoral=0x7f060041;
        public static final int LightCyan=0x7f060042;
        public static final int LightGoldenrodYellow=0x7f060043;
        public static final int LightGreen=0x7f060044;
        public static final int LightGrey=0x7f060045;
        public static final int LightPink=0x7f060046;
        public static final int LightSalmon=0x7f060047;
        public static final int LightSeaGreen=0x7f060048;
        public static final int LightSkyBlue=0x7f060049;
        public static final int LightSlateGray=0x7f06004a;
        public static final int LightSteelBlue=0x7f06004b;
        public static final int LightYellow=0x7f06004c;
        public static final int Lime=0x7f06004d;
        public static final int LimeGreen=0x7f06004e;
        public static final int Linen=0x7f06004f;
        public static final int Magenta=0x7f060050;
        public static final int Maroon=0x7f060051;
        public static final int MediumAquamarine=0x7f060052;
        public static final int MediumBlue=0x7f060053;
        public static final int MediumOrchid=0x7f060054;
        public static final int MediumPurple=0x7f060055;
        public static final int MediumSeaGreen=0x7f060056;
        public static final int MediumSlateBlue=0x7f060057;
        public static final int MediumSpringGreen=0x7f060058;
        public static final int MediumTurquoise=0x7f060059;
        public static final int MediumVioletRed=0x7f06005a;
        public static final int MidnightBlue=0x7f06005b;
        public static final int MintCream=0x7f06005c;
        public static final int MistyRose=0x7f06005d;
        public static final int Moccasin=0x7f06005e;
        public static final int NavajoWhite=0x7f06005f;
        public static final int Navy=0x7f060060;
        public static final int OldLace=0x7f060061;
        public static final int Olive=0x7f060062;
        public static final int OliveDrab=0x7f060063;
        public static final int Orange=0x7f060064;
        public static final int OrangeRed=0x7f060065;
        public static final int Orchid=0x7f060066;
        public static final int PaleGoldenrod=0x7f060067;
        public static final int PaleGreen=0x7f060068;
        public static final int PaleTurquoise=0x7f060069;
        public static final int PaleVioletRed=0x7f06006a;
        public static final int PapayaWhip=0x7f06006b;
        public static final int PeachPuff=0x7f06006c;
        public static final int Peru=0x7f06006d;
        public static final int Pink=0x7f06006e;
        public static final int Plum=0x7f06006f;
        public static final int PowderBlue=0x7f060070;
        public static final int Purple=0x7f060071;
        public static final int Red=0x7f060072;
        public static final int RosyBrown=0x7f060073;
        public static final int RoyalBlue=0x7f060074;
        public static final int SaddleBrown=0x7f060075;
        public static final int Salmon=0x7f060076;
        public static final int SandyBrown=0x7f060077;
        public static final int SeaGreen=0x7f060078;
        public static final int Seashell=0x7f060079;
        public static final int Sienna=0x7f06007a;
        public static final int Silver=0x7f06007b;
        public static final int SkyBlue=0x7f06007c;
        public static final int SlateBlue=0x7f06007d;
        public static final int SlateGray=0x7f06007e;
        public static final int Snow=0x7f06007f;
        public static final int SpringGreen=0x7f060080;
        public static final int SteelBlue=0x7f060081;
        public static final int Tan=0x7f060082;
        public static final int Teal=0x7f060083;
        public static final int Thistle=0x7f060084;
        public static final int Tomato=0x7f060085;
        public static final int Turquoise=0x7f060086;
        public static final int Violet=0x7f060087;
        public static final int Wheat=0x7f060088;
        public static final int White=0x7f060089;
        public static final int WhiteSmoke=0x7f06008a;
        public static final int Yellow=0x7f06008b;
        public static final int YellowGreen=0x7f06008c;
    }
    public static final class drawable {
        public static final int available_32x32=0x7f020000;
        public static final int available_48x48=0x7f020001;
        public static final int ic_menu_help=0x7f020002;
        public static final int ic_menu_info_details=0x7f020003;
        public static final int ic_menu_preferences=0x7f020004;
        public static final int ic_menu_save=0x7f020005;
        public static final int icon=0x7f020006;
        public static final int notavailable_32x32=0x7f020007;
        public static final int notavailable_48x48=0x7f020008;
    }
    public static final class id {
        public static final int SensorsListView=0x7f090020;
        public static final int aboutAccelRow0=0x7f090021;
        public static final int aboutAccelRow1=0x7f090023;
        public static final int aboutAccelRow10=0x7f090034;
        public static final int aboutAccelRow11=0x7f090036;
        public static final int aboutAccelRow12=0x7f090038;
        public static final int aboutAccelRow13=0x7f09003a;
        public static final int aboutAccelRow14=0x7f09003c;
        public static final int aboutAccelRow15=0x7f09003e;
        public static final int aboutAccelRow16=0x7f090040;
        public static final int aboutAccelRow2=0x7f090025;
        public static final int aboutAccelRow3=0x7f090027;
        public static final int aboutAccelRow4=0x7f090029;
        public static final int aboutAccelRow5=0x7f09002b;
        public static final int aboutAccelRow6=0x7f09002d;
        public static final int aboutAccelRow7=0x7f09002f;
        public static final int aboutAccelRow8=0x7f090031;
        public static final int aboutAccelRow9=0x7f090033;
        public static final int aboutApplicationLabelTextView=0x7f090008;
        public static final int aboutApplicationLabelValueTextView=0x7f09000a;
        public static final int aboutAuthorLabelTextView=0x7f090010;
        public static final int aboutAuthorLabelValueTextView=0x7f090012;
        public static final int aboutBatteryLabelTextView=0x7f090018;
        public static final int aboutBatteryLabelValueTextView=0x7f09001a;
        public static final int aboutDescriptionLabelTextView=0x7f09000c;
        public static final int aboutDescriptionLabelValueTextView=0x7f09000e;
        public static final int aboutGTLRow1=0x7f090007;
        public static final int aboutGTLRow10=0x7f090019;
        public static final int aboutGTLRow2=0x7f090009;
        public static final int aboutGTLRow3=0x7f09000b;
        public static final int aboutGTLRow4=0x7f09000d;
        public static final int aboutGTLRow5=0x7f09000f;
        public static final int aboutGTLRow6=0x7f090011;
        public static final int aboutGTLRow7=0x7f090013;
        public static final int aboutGTLRow8=0x7f090015;
        public static final int aboutGTLRow9=0x7f090017;
        public static final int aboutVersionLabelTextView=0x7f090014;
        public static final int aboutVersionLabelValueTextView=0x7f090016;
        public static final int accept_btn=0x7f090003;
        public static final int action_settings=0x7f090045;
        public static final int delimiter=0x7f090022;
        public static final int delimiter1=0x7f090026;
        public static final int details=0x7f09001e;
        public static final int deviceNameTextView=0x7f09001f;
        public static final int frameLayout1=0x7f090005;
        public static final int itemAbout=0x7f090042;
        public static final int license=0x7f090002;
        public static final int logoImageView=0x7f090006;
        public static final int name=0x7f09001d;
        public static final int refuse_btn=0x7f090004;
        public static final int scroller=0x7f090000;
        public static final int sensorAccuracyTitle=0x7f090032;
        public static final int sensorMaxRange=0x7f090030;
        public static final int sensorMinDelay=0x7f09002c;
        public static final int sensorName=0x7f090028;
        public static final int sensorNameValue=0x7f090024;
        public static final int sensorPower=0x7f09002a;
        public static final int sensorResolution=0x7f09002e;
        public static final int sensorValueW=0x7f09003b;
        public static final int sensorValueX=0x7f090035;
        public static final int sensorValueY=0x7f090037;
        public static final int sensorValueZ=0x7f090039;
        public static final int sensorValuebX=0x7f09003d;
        public static final int sensorValuebY=0x7f09003f;
        public static final int sensorValuebZ=0x7f090041;
        public static final int sensor_itemSave=0x7f090044;
        public static final int sensor_itemSettings=0x7f090043;
        public static final int status=0x7f09001b;
        public static final int textView1=0x7f090001;
        public static final int title=0x7f09001c;
    }
    public static final class layout {
        public static final int appmain=0x7f030000;
        public static final int info=0x7f030001;
        public static final int list_view_row=0x7f030002;
        public static final int main=0x7f030003;
        public static final int sensor_values=0x7f030004;
    }
    public static final class menu {
        public static final int optionsmenu=0x7f080000;
        public static final int sensor_values=0x7f080001;
    }
    public static final class string {
        public static final int about_application_label_title=0x7f070000;
        public static final int about_application_label_value=0x7f070001;
        public static final int about_author_label_title=0x7f070002;
        public static final int about_author_label_value=0x7f070003;
        public static final int about_batterylevel_text_title=0x7f070004;
        public static final int about_batterylevel_text_value=0x7f070005;
        public static final int about_description_label_title=0x7f070006;
        public static final int about_description_label_value=0x7f070007;
        public static final int about_version_text_title=0x7f070008;
        public static final int about_version_text_value=0x7f070009;
        public static final int accelerometer_accuracy_value=0x7f07000a;
        public static final int accelerometer_text_title=0x7f07000b;
        public static final int accelerometer_text_value=0x7f07000c;
        public static final int accuracy_text=0x7f07000d;
        public static final int action_settings=0x7f07000e;
        public static final int ambienttemperature_accuracy_value=0x7f07000f;
        public static final int ambienttemperature_text_title=0x7f070010;
        public static final int ambienttemperature_text_value=0x7f070011;
        public static final int app_name=0x7f070012;
        public static final int appmain_accept_btn=0x7f070013;
        public static final int appmain_copyright=0x7f070014;
        public static final int appmain_copyright_text=0x7f070015;
        public static final int appmain_disclaimer=0x7f070016;
        public static final int appmain_license_text=0x7f070017;
        public static final int appmain_refuse_btn=0x7f070018;
        public static final int author_nam_value=0x7f070019;
        public static final int author_name_title=0x7f07001a;
        public static final int battery_health_dead=0x7f07001b;
        public static final int battery_health_good=0x7f07001c;
        public static final int battery_health_notavailable=0x7f07001d;
        public static final int battery_health_overheat=0x7f07001e;
        public static final int battery_health_overvoltage=0x7f07001f;
        public static final int battery_health_unspecifiedfailure=0x7f070020;
        public static final int battery_status_charging=0x7f070021;
        public static final int battery_status_discharging=0x7f070022;
        public static final int battery_status_full=0x7f070023;
        public static final int battery_status_notavailable=0x7f070024;
        public static final int battery_status_notcharging=0x7f070025;
        public static final int battery_text=0x7f070026;
        public static final int deviceName_label_title=0x7f070027;
        public static final int doesnot_exists_text=0x7f070028;
        public static final int doesnot_found_sensor_bytype=0x7f070029;
        public static final int doesnot_matched_number_of_available_sensors=0x7f07002a;
        public static final int doesnot_registered_accelerometer_sensor_listener=0x7f07002b;
        public static final int doesnot_registered_ambienttemperature_sensor_listener=0x7f07002c;
        public static final int doesnot_registered_gamerotationvector_sensor_listener=0x7f07002d;
        public static final int doesnot_registered_geomagrotationvector_sensor_listener=0x7f07002e;
        public static final int doesnot_registered_gravity_sensor_listener=0x7f07002f;
        public static final int doesnot_registered_gyroscope_sensor_listener=0x7f070030;
        public static final int doesnot_registered_gyroscopeu_sensor_listener=0x7f070031;
        public static final int doesnot_registered_heartrate_sensor_listener=0x7f070032;
        public static final int doesnot_registered_light_sensor_listener=0x7f070033;
        public static final int doesnot_registered_linearacceleration_sensor_listener=0x7f070034;
        public static final int doesnot_registered_magneticfield_sensor_listener=0x7f070035;
        public static final int doesnot_registered_magneticfieldu_sensor_listener=0x7f070036;
        public static final int doesnot_registered_orientation_sensor_listener=0x7f070037;
        public static final int doesnot_registered_pressure_sensor_listener=0x7f070038;
        public static final int doesnot_registered_proximity_sensor_listener=0x7f070039;
        public static final int doesnot_registered_relativehumidity_sensor_listener=0x7f07003a;
        public static final int doesnot_registered_rotationvector_sensor_listener=0x7f07003b;
        public static final int doesnot_registered_sensor_listener=0x7f07003c;
        public static final int doesnot_registered_significantmotion_sensor_listener=0x7f07003d;
        public static final int doesnot_registered_stepcounter_sensor_listener=0x7f07003e;
        public static final int doesnot_registered_stepdetector_sensor_listener=0x7f07003f;
        public static final int doesnot_registered_temperature_sensor_listener=0x7f070040;
        public static final int error_externalstorage_media_bad_removal=0x7f070041;
        public static final int error_externalstorage_media_unmountable=0x7f070042;
        public static final int error_externalstorage_media_unmounted=0x7f070043;
        public static final int error_externalstorage_mounted_read_only=0x7f070044;
        public static final int error_externalstorage_nofs=0x7f070045;
        public static final int error_externalstorage_not_present=0x7f070046;
        public static final int error_qnh_value_invalid=0x7f070047;
        public static final int gamerotationvector_accuracy_value=0x7f070048;
        public static final int gamerotationvector_text_title=0x7f070049;
        public static final int gamerotationvector_text_value=0x7f07004a;
        public static final int geomagrotationvector_accuracy_value=0x7f07004b;
        public static final int geomagrotationvector_text_title=0x7f07004c;
        public static final int geomagrotationvector_text_value=0x7f07004d;
        public static final int gravity_accuracy_value=0x7f07004e;
        public static final int gravity_text_title=0x7f07004f;
        public static final int gravity_text_value=0x7f070050;
        public static final int gyroscope_accuracy_value=0x7f070051;
        public static final int gyroscope_text_title=0x7f070052;
        public static final int gyroscope_text_value=0x7f070053;
        public static final int heartrate_accuracy_value=0x7f070054;
        public static final int heartrate_text_title=0x7f070055;
        public static final int heartrate_text_value=0x7f070056;
        public static final int hello=0x7f070057;
        public static final int light_accuracy_value=0x7f070058;
        public static final int light_text_title=0x7f070059;
        public static final int light_text_value=0x7f07005a;
        public static final int linearacceleration_accuracy_value=0x7f07005b;
        public static final int linearacceleration_text_title=0x7f07005c;
        public static final int linearacceleration_text_value=0x7f07005d;
        public static final int magneticfield_accuracy_value=0x7f07005e;
        public static final int magneticfield_text_title=0x7f07005f;
        public static final int magneticfield_text_value=0x7f070060;
        public static final int main_menu_about=0x7f070061;
        public static final int main_menu_save=0x7f070062;
        public static final int main_menu_settings=0x7f070063;
        public static final int orientation_accuracy_value=0x7f070064;
        public static final int orientation_azimuth=0x7f070065;
        public static final int orientation_pitch=0x7f070066;
        public static final int orientation_roll=0x7f070067;
        public static final int orientation_text_title=0x7f070068;
        public static final int orientation_text_value=0x7f070069;
        public static final int preference_settings_title_text=0x7f07006a;
        public static final int presure_accuracy_value=0x7f07006b;
        public static final int presure_text_title=0x7f07006c;
        public static final int presure_text_value=0x7f07006d;
        public static final int proximity_accuracy_value=0x7f07006e;
        public static final int proximity_distance=0x7f07006f;
        public static final int proximity_text_title=0x7f070070;
        public static final int proximity_text_value=0x7f070071;
        public static final int relativehumidity_accuracy_value=0x7f070072;
        public static final int relativehumidity_text_title=0x7f070073;
        public static final int relativehumidity_text_value=0x7f070074;
        public static final int rotationvector_accuracy_value=0x7f070075;
        public static final int rotationvector_text_title=0x7f070076;
        public static final int rotationvector_text_value=0x7f070077;
        public static final int sensorAccuracyTitle=0x7f070078;
        public static final int sensorAccuracyValue=0x7f070079;
        public static final int sensorName=0x7f07007a;
        public static final int sensorValueW=0x7f07007b;
        public static final int sensorValueX=0x7f07007c;
        public static final int sensorValueY=0x7f07007d;
        public static final int sensorValueZ=0x7f07007e;
        public static final int sensor_accuracy_level_high=0x7f07007f;
        public static final int sensor_accuracy_level_low=0x7f070080;
        public static final int sensor_accuracy_level_medium=0x7f070081;
        public static final int sensor_accuracy_level_unreliable=0x7f070082;
        public static final int sensor_details_maxrange=0x7f070083;
        public static final int sensor_details_mindelay=0x7f070084;
        public static final int sensor_details_power=0x7f070085;
        public static final int sensor_details_resolution=0x7f070086;
        public static final int sensor_unknown_text_title=0x7f070087;
        public static final int settings_common_category_display=0x7f070088;
        public static final int settings_descr_common_category_display=0x7f070089;
        public static final int settings_descr_sensors_presure_qnh_value_title=0x7f07008a;
        public static final int settings_descr_sensorshowoptionkind_title=0x7f07008b;
        public static final int settings_sensors_presure_qnh_value_title=0x7f07008c;
        public static final int settings_sensorshowoptionkind_title=0x7f07008d;
        public static final int settings_take_effect=0x7f07008e;
        public static final int significantmotion_accuracy_value=0x7f07008f;
        public static final int significantmotion_text_title=0x7f070090;
        public static final int significantmotion_text_value=0x7f070091;
        public static final int status_descr=0x7f070092;
        public static final int stepcounter_accuracy_value=0x7f070093;
        public static final int stepcounter_text_title=0x7f070094;
        public static final int stepcounter_text_value=0x7f070095;
        public static final int stepdetector_accuracy_value=0x7f070096;
        public static final int stepdetector_text_title=0x7f070097;
        public static final int stepdetector_text_value=0x7f070098;
        public static final int temperature_accuracy_value=0x7f070099;
        public static final int temperature_text_title=0x7f07009a;
        public static final int temperature_text_value=0x7f07009b;
        public static final int title_activity_sensor_values=0x7f07009c;
        public static final int uncalibratedmagneticfield_accuracy_value=0x7f07009d;
        public static final int uncalibratedmagneticfield_text_title=0x7f07009e;
        public static final int uncalibratedmagneticfield_text_value=0x7f07009f;
        public static final int uncalibrategyroscope_accuracy_value=0x7f0700a0;
        public static final int uncalibrategyroscope_text_title=0x7f0700a1;
        public static final int uncalibrategyroscope_text_value=0x7f0700a2;
    }
    public static final class xml {
        public static final int sensor_preferences=0x7f040000;
    }
}
